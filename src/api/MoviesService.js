import axios from 'axios';

const API_KEY = 'c6fe306daec5934db4f328eb33fe38b5';
const BASE_URL = 'https://api.themoviedb.org/3/';
const withBaseUrl = path => `${BASE_URL}${path}?api_key=${API_KEY}`;

export class MoviesService {
    static getMovies() {
        return axios(withBaseUrl('movie/popular'));
    }

    static getMoviesId(id){
        return axios(withBaseUrl(`movie/${id}`))
    }
}