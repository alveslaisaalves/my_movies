import { MoviesContainer } from "../styles/MoviesContainer";
import { MovieItem } from "./MovieItem";

export const MoviesList = ({movies}) => (
    <MoviesContainer>
        <ul>
            {movies.map(m => <MovieItem movie={m} />)}
        </ul>
    </MoviesContainer>
        
);