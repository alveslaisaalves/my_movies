import { Header } from "./components/Header";
import { About } from "./views/About";
import { Home } from "./views/Home"
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { MovieDetail } from "./views/MovieDetail";

function App() {
  return (
    <div className="App">
      <Router>
        <Header />
        <Switch>
          <Route  exact component={Home}  path="/">
            <Home />
          </Route>
          <Route  exact path="/movie/:id">
            <MovieDetail />
          </Route>
          <Route  exact component={About}  path="/about">
            <About />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
