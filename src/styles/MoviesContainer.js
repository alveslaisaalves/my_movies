import styled from 'styled-components';

export const MoviesContainer = styled.section`
    padding: 15px;
    margin: 0 20px 20px 20px;
    background: #cecece;
    border: 1px solid black;
    border-radius: 10px;
`;

